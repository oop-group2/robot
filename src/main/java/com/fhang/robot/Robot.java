/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.robot;

/**
 *
 * @author Natthakritta
 */
public class Robot {
     private int rx;
    private int ry;
    private int bx;
    private int by;
    private int N;
    private char lastdirection;
    
    public Robot(int rx,int ry,int bx,int by,int N){
        this.rx = rx;
        this.ry = ry;
        this.bx = bx;
        this.by = by;
        this.N = N;
    }
    public boolean inMap (int rx,int ry){
        if (rx>=N ||rx<0 || ry>=N || ry<0) {
            return false;
        }
        return true;
    }
    public boolean walk(char direction){
        switch(direction){
         
            case'N':
                if(!inMap(rx,ry+1)){
                    printUnableMove();
                    return false;
                }
                ry = ry+1;
                break;
            case'S':
                if(!inMap(rx,ry-1)){
                    printUnableMove();
                    return false;
                }
                ry = ry-1;
                break;
            case'E':
                if(!inMap(rx+1,ry)){
                    printUnableMove();
                    return false;
                }
                rx = rx+1;
                break;
            case'W':
                if(!inMap(rx-1,ry)){
                    printUnableMove();
                    return false;
                }
                rx = rx-1;
                break;
        }
        lastdirection=direction;
        if (isBomb()) {
            printBombfound();
        }
    return true;
    }
    public boolean walk(char direction,int step){
        for (int i = 0; i < step; i++) {
            if (!this.walk(direction)) {
                return false;
            }
        }
        return true;
    }
     public boolean walk(){
         return this.walk(lastdirection);
     }
     public boolean walk(int step){
         return this.walk(lastdirection,step);
     }
    public String toString(){
        return "Robot ("+this.rx+" , "+this.ry+")";
    }
    public void printUnableMove(){
        System.out.println("I can't move");
    }
    public void printBombfound(){
            System.out.println("Bombfound!!!");
}
     
    public boolean isBomb(){
        if (rx==bx && ry==by) {
            return true;
        }
        return false;
    }
}
