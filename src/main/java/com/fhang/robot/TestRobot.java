/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.robot;

/**
 *
 * @author Natthakritta
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot = new Robot(0,0,2,2,100);
        System.out.println(robot);
        robot.walk('N');
        System.out.println(robot);
        robot.walk('S');
        robot.walk('S');
        System.out.println(robot);
        robot.walk('E');
        System.out.println(robot);
        robot.walk('W');
        System.out.println(robot);
        robot.walk('E',3);
        System.out.println(robot);
        robot.walk();
        System.out.println(robot);
        robot.walk(4);
        System.out.println(robot);
        robot.walk('N',101);
        System.out.println(robot);
        robot.walk('S',100);
        System.out.println(robot);
        robot.walk('E',92);
        System.out.println(robot);
        robot.walk('W',100);
        System.out.println(robot);
         robot.walk('N',2);
        System.out.println(robot);
        robot.walk('E',2);
        System.out.println(robot);
    }
       
      
}
